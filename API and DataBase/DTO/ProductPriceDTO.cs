﻿namespace API_and_DataBase.DTO
{
    public class ProductPriceDTO
    {
        public decimal buyingPrice { get; set; }    
        public decimal SellingPrice { get; set; }
    }
}
